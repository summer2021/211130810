
package coderxz.scdubbo;

import coderxz.service.ISayHelloService;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author: coderxz
 * @create: 2021/7/7 17:23
 */
@RestController
@Slf4j(topic = "c.dubboClient")
public class DubboController {

    //当我调用远程服务失败后，我需要选择一个备用的方案返回给客户端   注意不能使用failsafe，这玩意会把错误吞掉
    @Reference(loadbalance = "roundrobin",
            timeout = 1,cluster = "failfast",check = false)
    ISayHelloService iSayHelloService;

    @RequestMapping("sayhello")
    public String sayHello() throws InterruptedException {
        log.debug("coderxz:sayhello");
        return iSayHelloService.sayHello();
    }
}