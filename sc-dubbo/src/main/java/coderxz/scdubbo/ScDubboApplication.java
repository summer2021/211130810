package coderxz.scdubbo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class ScDubboApplication {

    public static void main(String[] args) {
        SpringApplication.run(ScDubboApplication.class, args);
    }

}
