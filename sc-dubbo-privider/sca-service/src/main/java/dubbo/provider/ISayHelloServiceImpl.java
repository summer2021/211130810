package dubbo.provider;

import coderxz.service.ISayHelloService;
import org.apache.dubbo.config.annotation.Service;

import java.util.concurrent.TimeUnit;

/**
 * @author: coderxz
 * @create: 2021/8/9 10:42
 */
@Service(loadbalance = "roundrobin",weight = 10,cluster = "failsafe")
public class ISayHelloServiceImpl implements ISayHelloService {
    @Override
    public String sayHello() throws InterruptedException {
        System.out.println("coderxz:hello");
        //模拟请求有延迟的情况
        TimeUnit.SECONDS.sleep(1);
        return "hello coderxz dubbo第一次的测试";
    }
}
