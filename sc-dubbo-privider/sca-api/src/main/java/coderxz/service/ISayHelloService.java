package coderxz.service;

/**
 * @author: coderxz
 * @create: 2021/8/9 10:41
 */
public interface ISayHelloService {
    String sayHello() throws InterruptedException;
}
